# Migration notes

## TableSchema

### descriptor

before

```python
fields = shema.descriptor.get('fields')
```

now

```python
fields = schema.get('fields')
```

## Tabulator

### Tabulator stream

before

```python
import tabulator

def consume_source(source, **options)
    stream = tabulator.stream.Stream(source, **options)
    stream.open()

    # Get source headers
    headers = next(stream.iter())
    # And source body rows
    body_rows = list(stream.iter())

    return headers, body_rows
```

now

```python
from frictionless import Table

def consume_source(source, **options)
    stream = Table(source, **options)
    stream.open()
    # Get source headers
    headers = stream.header
    # And source body rows
    body_rows = list(stream.data_stream)

    return headers, body_rows
```

### Tabulator loader

- `tabulator.loader.Loader` base class is replaced by `frictionless.Loader`
- methods to implement have changed

## Goodtables

### Custom checks

- `goodtables.registry.check` is no more used to decorate a custom check class
- custom check classes have to subclass `frictionless.Check` base class
- methods to implement have changed

### frictionless.Plugin

The new way to add custom checks, controls, dialects, loaders, parsers, servers

### Error

- `goodtables.Error` class is replaced by `frictionless.errors.Error` base class
- all the other errors are subclasses, either of `frictionless.errors.Error` directly
  or other errors like `frictionless.errors.SourceError` or `frictionless.errors.HeaderError`
- Each error class defines 5 properties: `code`, `name`, `tags`, `template` and `description`, e.g.:

```python
class ExtraHeaderError(HeaderError):
    code = "extra-header"
    name = "Extra Header"
    tags = ["#head", "#structure"]
    template = 'There is an extra header "{cell}" in field at position "{fieldPosition}"'
    description = "The first row of the data source contains header that does not exist in the schema."
```

- Error constructor takes 5 named parameters: `note`, `cell`, `cells`, `field_name` and `field_position`
- compared to `goodtables` version, the new error system is **much less flexible**:
  - error message templates are hard-coded within error classes
  - template variables are restricted to 6 pre-defined values:
    - passed as constructor parameter: `cell` (cell value), `fieldName`, `fieldPosition`, `fieldNumber`, `note` (free text)
    - or provided by the engine: `rowPosition`

### Inspector

- `Inspector.inspect` function is replaced by `frictionless.validate_table`
- `order_fields` option is replaced by `sync_schema`
- `error_limit` option is replaced by `limit_errors`
- `checks` option is replaced by `pick/skip_errors` and `extra_checks`
