import logging
from pathlib import Path

import pytest
import frictionless.errors as errors
from frictionless import validate_table, Schema, Table
from frictionless.exceptions import FrictionlessException

script_dir = Path(__file__).resolve().parent

log = logging.Logger(__name__)


@pytest.fixture
def simple_contact_schema_path():
    return script_dir / "fixtures" / "simple_contact_schema.json"


@pytest.fixture
def simple_contact_csv_path():
    return script_dir / "fixtures" / "simple_contact.csv"


def test_simple_schema(simple_contact_schema_path):
    assert simple_contact_schema_path.exists()
    schema = Schema(str(simple_contact_schema_path))


def test_get_descriptor(simple_contact_schema_path):
    schema = Schema(str(simple_contact_schema_path))
    try:
        # There's no schema.descriptor anymore
        field_list = schema.descriptor.get("fields")
        assert False
    except AttributeError:
        pass

    # Descriptor can be directly accessed from schema instance
    field_list = schema.get("fields")


def test_validate_csv(simple_contact_schema_path, simple_contact_csv_path):
    schema = Schema(str(simple_contact_schema_path))

    # validate_table replaces inspector.inspect
    report = validate_table(str(simple_contact_csv_path), schema=schema)


def test_tabulator_stream(simple_contact_csv_path):
    # Below code replaces
    # stream = tabulator.stream.Stream(source, **options)
    # stream.open()
    # headers = next(stream.iter())
    # body_rows = list(stream.iter())
    stream = Table(str(simple_contact_csv_path))
    stream.open()
    headers = stream.header
    assert len(headers) == 3
    body_rows = list(stream.data_stream)
    assert len(body_rows) == 2
    for row in body_rows:
        assert len(row) == len(headers)


def test_error_class():
    # err = Error(code="extra-header", message_substitutions={"column-name": "foo"})
    err = errors.ExtraHeaderError(
        note="This is a note",
        cells=[],
        cell="cell content",
        field_name="foobar",
        field_number=7,
        field_position=4,
    )